import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import utils.PostRequest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static io.appium.java_client.touch.offset.PointOption.point;

public class DetectCall {
    WebDriver driver;
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;

    By call_log_home = By.id("com.samsung.android.dialer:id/collapsing_appbar_extended_title");
    By call_log_time = By.id("com.samsung.android.dialer:id/text_call_log_time");
    //By call_log_date = By.id("com.samsung.android.dialer:id/text_call_log_header");
    By caller_number = By.id("com.samsung.android.dialer:id/text_call_log_main");

    //============= User Input =============
    String api_end_point = "https://appiumapi.herokuapp.com/appium/uploadFiles";
    //For disconnecting the incoming call you have to put the x and y coordinate of the call screen
    int x_coordinate = 833;
    int y_coordinate = 1911;
    //=======================================

    @BeforeTest
    public void runAppiumServer() throws IOException {
        System.out.println("&& Welcome to the call detector BOT &&");
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
        appiumServiceUrl = appiumService.getUrl().toString();
        appiumService.clearOutPutStreams();

        System.out.println("@@ The BOT is ready to go @@");

        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("noReset", "true");
        dc.setCapability("autoGrantPermissions", "true");
        dc.setCapability("appPackage", "com.samsung.android.dialer");
        dc.setCapability("appActivity", "com.samsung.android.dialer.DialtactsActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
    }


    @Test
    public void dtcCll() {
        waitForVisibilityOf(call_log_home);

        while (true){
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
            SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MMM-yyyy");
            Date date = new Date();
            String ss_name = "incoming_call_"+formatter.format(date);
            Calendar now = Calendar.getInstance();
            TimeZone timeZone = now.getTimeZone();
            String time_zone = timeZone.getID();

            System.out.println("No incoming call detect yet!");

            try {
                while (true){
                    if (driver.findElements(call_log_home).size() > 0){
                        //System.out.println("No incoming call detect yet!");
                        //Thread.sleep(1000);
                    }else {
                        System.out.println("Incoming call detected");
                        //new TouchAction((AndroidDriver)driver).tap(point(453, 247)).perform();

                        Thread.sleep(500);
                        capture(driver, ss_name);
                        System.out.println("Took screenshot");

                        (new TouchAction((PerformsTouchActions) driver)).press(PointOption.point(x_coordinate,y_coordinate))
                                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
                                .moveTo(PointOption.point(571,653)).release().perform();

                        System.out.println("Call disconnected");

                        waitForVisibilityOf(call_log_home);
                        break;
                    }
                }
                //System.out.println("Out of the loop!!");
                String caller_number_str = driver.findElement(caller_number).getText();
                String call_time = driver.findElement(call_log_time).getText();
                String call_date = formatter2.format(date);

                System.out.println("Caller number: "+caller_number_str);
                System.out.println("Date: "+formatter2.format(date));
                System.out.println("Time: "+call_time);
                System.out.println("Time zone: "+time_zone);

                String ss_name_call_log = "call_log_"+formatter.format(date);

                capture(driver, ss_name_call_log);

                String call_log_ss_path = System.getProperty("user.dir") + "\\screenshots\\"+ss_name_call_log+".png";
                String call_screen_ss_path = System.getProperty("user.dir") + "\\screenshots\\"+ss_name+".png";

                PostRequest.uploadImage(api_end_point, call_screen_ss_path, call_log_ss_path, caller_number_str, call_date, call_time, time_zone);
                System.out.println("Screenshots, caller number, Timezone, and Call time uploaded successfully!");

                System.out.println("================= (0_0) Looking for next incoming call ===================");
            }
            catch (Exception E) {
                System.out.println("Oops something went wrong!");
            }
        }
    }

    @AfterTest
    public void endBot() {
        driver.quit();
        appiumService.stop();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static String capture(WebDriver driver, String screenShotName) throws IOException {
        TakesScreenshot ts = (TakesScreenshot)driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String dest = System.getProperty("user.dir") + "\\screenshots\\" +screenShotName+ ".png";
        File destination = new File(dest);
        FileUtils.copyFile(source, destination);

        return dest;
    }

}
