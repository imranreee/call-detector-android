package utils;

import okhttp3.*;

import java.io.File;
import java.io.IOException;
import org.json.JSONObject;

public class PostRequest {
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    public static final OkHttpClient client = new OkHttpClient();

    public static void uploadImage(String api_end_point, String image_path_call_screen, String image_path_call_log, String phone_number, String call_date, String call_time, String time_zone) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone_number", phone_number);
        jsonObject.put("call_date", call_date);
        jsonObject.put("call_time", call_time);
        jsonObject.put("time_zone", time_zone);

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("jsonPayload", jsonObject.toString())
                .addFormDataPart("files", "call_log.png",
                        RequestBody.create(MEDIA_TYPE_PNG, new File(image_path_call_screen)))
                .addFormDataPart("files", "call_screen.png",
                        RequestBody.create(MEDIA_TYPE_PNG, new File(image_path_call_log)))
                .build();

        Request request = new Request.Builder()
                .url(api_end_point)
                .post(requestBody)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            //System.out.println(response.body().string());
        }
        return;
    }
}
